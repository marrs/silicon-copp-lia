package org.bitbucket.marrs.sico.actor.lib.input;

import static java.util.stream.Collectors.joining;
import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.bitbucket.marrs.sico.actor.lib.test.ActorBaseTest;
import org.bitbucket.marrs.sico.actor.lib.test.PortListener;
import org.bitbucket.marrs.sico.itest.ModelExecutionTest;
import org.junit.Test;

import ptolemy.data.DoubleToken;
import ptolemy.data.RecordToken;
import ptolemy.data.StringToken;
import ptolemy.data.Token;
import ptolemy.kernel.util.KernelException;

public class JsonEncoderTest extends ActorBaseTest {
    private static final String INPUT = new BufferedReader(new InputStreamReader(ModelExecutionTest.class.getResourceAsStream("/input.json")))
            .lines()
            .collect(joining());

    @Test
    public void test() throws KernelException {
        JsonEncoder jsonInput = new JsonEncoder(getContainer(), "testSimilarity");

        // Don't use normal quotes as the string in StringToken is escaped
        feedConstantInput(jsonInput, jsonInput.rawInput, new StringToken(INPUT));

        PortListener<Token> outputFeatures = terminateOutput(jsonInput, jsonInput.featuresOutput);
        PortListener<Token> outputWeights = terminateOutput(jsonInput, jsonInput.weightParametersOutput);
        PortListener<Token> outputEpistemics = terminateOutput(jsonInput, jsonInput.beliefsOutput);

        allowDisconnectedGraphs();
        executeModel();

        // TODO add test cases
        RecordToken featureRecord = (RecordToken) outputFeatures.getToken();
        RecordToken features = (RecordToken) featureRecord.get(FeaturesPort.FEATURES.name);
        assertEquals(((DoubleToken) features.get("blond hair")).doubleValue(), 1.0, 1e-10);

        // TODO add test cases
        RecordToken befiefsRecord = (RecordToken) outputEpistemics.getToken();
        RecordToken goals = (RecordToken) befiefsRecord.get(BeliefPort.GOAL_AMBITIONS.name);
        assertEquals(((DoubleToken) goals.get("prosperity")).doubleValue(), 0.75, 1e-10);

        // TODO add test cases
        RecordToken weightsRecord = (RecordToken) outputWeights.getToken();
        DoubleToken idtWeight = (DoubleToken) weightsRecord.get(WeightParameterPort.IDT_WEIGHT.name);
        assertEquals(idtWeight.doubleValue(), 0.5, 1e-10);
    }
}
