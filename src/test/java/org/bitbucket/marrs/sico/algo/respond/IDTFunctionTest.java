package org.bitbucket.marrs.sico.algo.respond;

import static junit.framework.Assert.assertEquals;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;


public final class IDTFunctionTest {
    private static final double DELTA = 1e-15;
    private static final long SEED = 2_603_886_880_581_722L;

    private Random random;

    @Before
    public void setupSeed() {
        random = new Random(SEED);
    }

    @Test
    public void equalWeightEqualValues() {
        IDTFunction idt = new IDTFunction(0.5);

        assertEquals(0.5, idt.apply(0.5, 0.5), DELTA);
    }

    @Test
    public void equalWeightInvolvmentDistanceBoundaries() {
        IDTFunction idt = new IDTFunction(0.5);

        assertEquals(0.75, idt.apply(0, 1), DELTA);
        assertEquals(0.0, idt.apply(0, 0), DELTA);
        assertEquals(1.0, idt.apply(1, 1), DELTA);
    }

    @Test
    public void weightBoundaries() {
        IDTFunction idtZero = new IDTFunction(0);

        // we expect the average
        assertEquals(0.5, idtZero.apply(0, 1), DELTA);
        assertEquals(0.25, idtZero.apply(0, 0.5), DELTA);
        assertEquals(0.75/2, idtZero.apply(0.25, 0.5), DELTA);
        assertEquals(0.5, idtZero.apply(0.5, 0.5), DELTA);

        IDTFunction idtOne = new IDTFunction(1);

        // we expect the maximum
        assertEquals(1.0, idtOne.apply(0, 1), DELTA);
        assertEquals(0.5, idtOne.apply(0, 0.5), DELTA);
        assertEquals(0.5, idtOne.apply(0.25, 0.5), DELTA);
        assertEquals(0.5, idtOne.apply(0.5, 0.5), DELTA);
    }

    @Test
    public void symetry() {
        for (int i = 0; i < 100; i++) {
            IDTFunction idtWeightHalf = new IDTFunction(random.nextDouble());

            for (int j = 0; j < 100; j++) {
                double left = random.nextDouble();
                double right = random.nextDouble();

                assertEquals("Failed with seed: " + SEED,
                    idtWeightHalf.apply(left, right), idtWeightHalf.apply(right, left), DELTA);
            }
        }
    }
}
