package org.bitbucket.marrs.sico.algo.compare;

import static java.lang.Math.sqrt;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import org.junit.Before;
import org.junit.Test;

public final class SimilarityDistanceFunctionTest {

    private static final double DELTA = 1e-15;
    private static final long SEED = 2_603_886_880_581_722L;

    private Random random;

    @Before
    public void setupSeed() {
        random = new Random(SEED);
    }

    @Test
    public void featureInSymetricDifferenceForFeatureUnion() {
        SimilarityDistanceFunction distance = SimilarityDistanceFunction.forFeatureUnion(new double[]{ 1, 1 });

        RealVector unitX = new ArrayRealVector(new double[]{ 0.0, 0.0 });
        RealVector unitY = new ArrayRealVector(new double[]{ 0.0, 0.0 });

        assertEquals(0.0, distance.apply(unitX, unitY, true, false), 0.0);
        assertEquals(0.0, distance.apply(unitX, unitY, false, true), 0.0);
        assertEquals(0.0, distance.apply(unitX, unitY, true, true), 0.0);
        assertEquals(0.0, distance.apply(unitX, unitY, false, false), 0.0);
    }

    @Test
    public void featureInSymetricDifferenceForFeatureIntersection() {
        SimilarityDistanceFunction distance = SimilarityDistanceFunction.forFeatureIntersection(new double[]{ 1, 1 });

        RealVector unitY = new ArrayRealVector(new double[]{ 0.0, 0.0 });
        RealVector unitX = new ArrayRealVector(new double[]{ 0.0, 0.0 });

        assertEquals(1.0, distance.apply(unitX, unitY, true, false), 0.0);
        assertEquals(1.0, distance.apply(unitX, unitY, false, true), 0.0);
        assertEquals(0.0, distance.apply(unitX, unitY, true, true), 0.0);
        assertEquals(0.0, distance.apply(unitX, unitY, false, false), 0.0);
    }

    @Test
    public void asNormalizedEucledianDistanceIn2D() {
        SimilarityDistanceFunction distance = SimilarityDistanceFunction.forFeatureIntersection(new double[]{ 1, 1 });

        RealVector zero = new ArrayRealVector(2, 0.0);
        assertEquals(0.0, distance.apply(zero, zero), DELTA);

        RealVector unitX = new ArrayRealVector(new double[]{ 1.0, 0.0 });
        RealVector unitY = new ArrayRealVector(new double[]{ 0.0, 1.0 });
        assertEquals(1/sqrt(2), distance.apply(unitX, zero), DELTA);
        assertEquals(1, distance.apply(unitX, unitY), DELTA);

        for (int i = 0; i < 10000; i++) {
            RealVector vecX = new ArrayRealVector(randomDoubleArray(2));
            RealVector vecY = new ArrayRealVector(randomDoubleArray(2));
            assertEquals(1/sqrt(2) * vecX.getDistance(vecY), distance.apply(vecX, vecY), DELTA);
        }
    }

    @Test
    public void distanceIsBetweenZeroAndOneForAllWeights() {
        for (int i = 0; i < 10000; i++) {
            SimilarityDistanceFunction distance = SimilarityDistanceFunction.forFeatureIntersection(randomDoubleArray(10));

            RealVector vecX = new ArrayRealVector(randomDoubleArray(10));
            RealVector vecY = new ArrayRealVector(randomDoubleArray(10));
            assertTrue(distance.apply(vecX, vecY) >= 0.0);
            assertTrue(distance.apply(vecX, vecY) <= 1.0);
        }
    }

    @Test
    public void distanceIsSymetric() {
        for (int i = 0; i < 10000; i++) {
            SimilarityDistanceFunction distance = SimilarityDistanceFunction.forFeatureIntersection(randomDoubleArray(10));

            RealVector vecX = new ArrayRealVector(randomDoubleArray(10));
            RealVector vecY = new ArrayRealVector(randomDoubleArray(10));
            assertEquals(distance.apply(vecX, vecY), distance.apply(vecY, vecX), DELTA);
        }
    }

    @Test(expected=IllegalArgumentException.class)
    public void checksArgumentsAreWeightsForOtherX() {
        SimilarityDistanceFunction distance = SimilarityDistanceFunction.forFeatureIntersection(new double[]{ 1, 1 });

        RealVector unitX = new ArrayRealVector(new double[]{ 1.1, 0.0 });
        RealVector unitY = new ArrayRealVector(new double[]{ 0.0, 0.0 });

        distance.apply(unitX, unitY);
    }

    @Test(expected=IllegalArgumentException.class)
    public void checksArgumentsAreWeightsForOtherY() {
        SimilarityDistanceFunction distance = SimilarityDistanceFunction.forFeatureIntersection(new double[]{ 1, 1 });

        RealVector unitX = new ArrayRealVector(new double[]{ 0.0, -0.1 });
        RealVector unitY = new ArrayRealVector(new double[]{ 0.0, 0.0 });

        distance.apply(unitX, unitY);
    }

    @Test(expected=IllegalArgumentException.class)
    public void checksArgumentsAreWeightsForSelfX() {
        SimilarityDistanceFunction distance = SimilarityDistanceFunction.forFeatureIntersection(new double[]{ 1, 1 });

        RealVector unitX = new ArrayRealVector(new double[]{ 0.0, 0.0 });
        RealVector unitY = new ArrayRealVector(new double[]{ -0.1, 0.0 });

        distance.apply(unitX, unitY);
    }

    @Test(expected=IllegalArgumentException.class)
    public void checksArgumentsAreWeightsForSelfY() {
        SimilarityDistanceFunction distance = SimilarityDistanceFunction.forFeatureIntersection(new double[]{ 1, 1 });

        RealVector unitX = new ArrayRealVector(new double[]{ 0.0, 0.0 });
        RealVector unitY = new ArrayRealVector(new double[]{ 0.0, 1.1 });

        distance.apply(unitX, unitY);
    }

    private double[] randomDoubleArray(int size) {
        double[] result = new double[size];
        for (int i = 0; i < result.length; i++) {
            result[i] = random.nextDouble();
        }

        return result;
    }
}
