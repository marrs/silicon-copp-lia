package org.bitbucket.marrs.sico.rest;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.client.utils.URIBuilder;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.google.common.collect.ImmutableMap;

public class RestGetTest {

    @Rule
    public WireMockRule httpServerMockRule = new WireMockRule(8089);

    private RestGet restGet;

    @Before
    public void setUp() throws URISyntaxException {
        restGet = new RestGet(new URIBuilder("http://localhost:8089/test").build());
    }

    @Test
    public void requestExecutes() throws IOException {
        stubFor(get(urlEqualTo("/test"))
            .willReturn(aResponse()
                .withStatus(299)
                .withBody("")));

        restGet.get();

       verify(getRequestedFor(urlEqualTo("/test")));
    }

    @Test
    public void requestAcceptsJson() throws IOException {
        stubFor(get(urlEqualTo("/test"))
            .willReturn(aResponse()
                .withStatus(200)
                .withBody("")));

        restGet.get();

        verify(getRequestedFor(urlEqualTo("/test"))
            .withHeader("Accept", containing("application/json")));
    }

    @Test(expected=IOException.class)
    public void throwsOnStatusGreaterThan300() throws IOException {
        stubFor(get(urlEqualTo("/test"))
            .willReturn(aResponse()
                .withStatus(300)
                .withBody("")));

        restGet.get();
    }

    @Test
    public void addsParameters() throws IOException {
        stubFor(get(urlPathEqualTo("/test"))
            .willReturn(aResponse()
                .withStatus(200)
                .withBody("")));

        restGet.get(ImmutableMap.of("parameter1", "value1", "parameter2", "value2"));

        verify(getRequestedFor(urlPathEqualTo("/test"))
            .withQueryParam("parameter1", equalTo("value1"))
            .withQueryParam("parameter2", equalTo("value2")));
    }

    @Test
    public void parsesResponse() throws IOException {
        stubFor(get(urlPathEqualTo("/test"))
            .willReturn(aResponse()
                .withStatus(200)
                .withBody("{Test\tresponse\nwith\nmultiple\nlines and: 'special' characters \"}")));

        assertEquals("{Test\tresponse\nwith\nmultiple\nlines and: 'special' characters \"}", restGet.get());
    }
}
