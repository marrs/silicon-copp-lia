package org.bitbucket.marrs.sico.actor.lib.input;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Arrays.asList;

import org.bitbucket.marrs.sico.data.token.FuzzyWeightToken;

import ptolemy.data.DoubleToken;
import ptolemy.kernel.util.IllegalActionException;

final class Feature {
    private final String name;
    private final Double weight;

    private final Double aesthPos;
    private final Double aesthNeg;

    private final Double ethPos;
    private final Double ethNeg;

    private final Double epPos;
    private final Double epNeg;

    private final Double affPos;
    private final Double affNeg;

    Feature(String name, Double weight, Double aesthPos, Double aesthNeg, Double ethPos,
            Double ethNeg, Double epPos, Double epNeg, Double affPos, Double affNeg) {
        this.name = name;
        this.weight = weight;
        this.aesthPos = aesthPos;
        this.aesthNeg = aesthNeg;
        this.ethPos = ethPos;
        this.ethNeg = ethNeg;
        this.epPos = epPos;
        this.epNeg = epNeg;
        this.affPos = affPos;
        this.affNeg = affNeg;
    }

    void validate() {
        asList(weight, aesthNeg, ethPos, ethNeg, epPos, epNeg, affPos, affNeg).stream()
                .forEach(this::validate);
    }

    private void validate(Double weightValue) {
        checkArgument(weightValue != null, "weightValue must be non-null");
        checkArgument(weightValue >= 0.0 && weightValue <= 1.0, "Illegal weight value: " + weightValue);
    }

    String getName() {
        return name;
    }

    DoubleToken getWeight() {
        return new DoubleToken(weight);
    }

    FuzzyWeightToken getAestheticsWeights() throws IllegalActionException {
        return new FuzzyWeightToken(aesthPos, aesthNeg);
    }

    FuzzyWeightToken getEthicsWeights() throws IllegalActionException {
        return new FuzzyWeightToken(ethPos, ethNeg);
    }

    FuzzyWeightToken getEpistemicsWeights() throws IllegalActionException {
        return new FuzzyWeightToken(epPos, epNeg);
    }

    FuzzyWeightToken getAffordancesWeights() throws IllegalActionException {
        return new FuzzyWeightToken(affPos, affNeg);
    }
}
