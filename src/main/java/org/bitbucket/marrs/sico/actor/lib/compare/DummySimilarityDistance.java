package org.bitbucket.marrs.sico.actor.lib.compare;

import static org.bitbucket.marrs.sico.actor.lib.util.FuzzyWeightCalculator.calculateAverage;
import static org.bitbucket.marrs.sico.data.type.FuzzyWeightType.FUZZY_WEIGHT;
import static ptolemy.data.DoubleToken.ONE;
import static ptolemy.data.DoubleToken.ZERO;

import java.util.Collection;
import java.util.List;

import org.bitbucket.marrs.sico.actor.lib.base.MultiRecordTransformation;
import org.bitbucket.marrs.sico.data.token.FuzzyWeightToken;
import org.bitbucket.marrs.sico.data.type.FuzzyWeightType;

import com.google.common.collect.ImmutableList;

import ptolemy.actor.TypedIOPort;
import ptolemy.data.Token;
import ptolemy.data.expr.Parameter;
import ptolemy.kernel.CompositeEntity;
import ptolemy.kernel.util.IllegalActionException;
import ptolemy.kernel.util.NameDuplicationException;

/**
 * Similarity distance calculates the similarity and dissimilarity between the
 * features input and a feature input for self.
 * <p>
 * For features present both in the input feature set and the feature set of
 * self, distance is calculated based on euclidean distance between the
 * indicative and counter-indicative values of the features, and are fed with
 * externally provided weights into similarity and dissimilarity.
 * <p>
 * Features that are only present in one of the feature set feed only into
 * dissimilarity.
 * <p>
 * The output is a token of {@link FuzzyWeightType} containing similarity as
 * indicative and dissimilarity as counter-indicative value.
 */
public final class DummySimilarityDistance extends MultiRecordTransformation<FuzzyWeightToken> {
    private static final List<String> INPUTS = ImmutableList.of("features");

    public Parameter similarity;
    public TypedIOPort featuresInput;

    public DummySimilarityDistance(CompositeEntity container, String name) throws NameDuplicationException, IllegalActionException {
        super(container, name, INPUTS, FUZZY_WEIGHT, FUZZY_WEIGHT);

        similarity = new Parameter(this, "similarity");
        similarity.setDisplayName("Similarity");
        similarity.setExpression("1.0");

        featuresInput = getInput(0);
    }

    @Override
    protected Transformation<Token, FuzzyWeightToken> getTransformation() throws IllegalActionException {
        return (label, args) -> {
                return new FuzzyWeightToken(ZERO.add(similarity.getToken()), ONE.subtract(similarity.getToken()));
        };
    }

    @Override
    protected Token aggregateOutput(Collection<FuzzyWeightToken> outputs) throws IllegalActionException {
        return calculateAverage(outputs);
    }

    public TypedIOPort getSimilarityOutput() {
        return output;
    }

    public TypedIOPort getSimilarityOutputTotal() {
        return getOutputAggregate();
    }
}
