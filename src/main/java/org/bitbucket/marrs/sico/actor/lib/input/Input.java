package org.bitbucket.marrs.sico.actor.lib.input;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.List;

public final class Input {
    private final List<Feature> features;
    private final Beliefs beliefs;
    private final WeightParameters weightParameters;

    Input(List<Feature> features, Beliefs beliefs, WeightParameters weightParameters) {
        checkArgument(features != null, "features must not be null");
        checkArgument(beliefs != null, "beliefs must not be null");
        checkArgument(weightParameters != null, "weightParameters must not be null");

        this.features = features;
        this.beliefs = beliefs;
        this.weightParameters = weightParameters;
    }

    List<Feature> getFeatures() {
        return features;
    }

    Beliefs getBeliefs() {
        return beliefs;
    }

    WeightParameters getWeightParameters() {
        return weightParameters;
    }
}
