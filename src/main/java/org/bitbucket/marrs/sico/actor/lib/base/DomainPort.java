package org.bitbucket.marrs.sico.actor.lib.base;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.Function;

public enum DomainPort {
    ETHICS("ethics", "Ethics"),
    AFFORDANCES("affordances", "Affordances"),
    AESTHETICS("aesthetics", "Aesthetics"),
    EPISTEMICS("epistemics", "Epistemics");

    public final String inputName;
    public final String inputNameTotal;
    public final String inputDisplayName;
    public final String inputDisplayNameTotal;
    public final String outputName;
    public final String outputNameTotal;
    public final String outputDisplayName;
    public final String outputDisplayNameTotal;

    DomainPort(String fieldName, String displayName) {
        this.inputName = fieldName + "Input";
        this.inputNameTotal = fieldName + "InputTotal";
        this.inputDisplayName = displayName;
        this.inputDisplayNameTotal = displayName + " total";
        this.outputName = fieldName + "Output";
        this.outputNameTotal = fieldName + "OutputTotal";
        this.outputDisplayName = displayName;
        this.outputDisplayNameTotal = displayName + " total";
    }

    public static List<String> inputNames() {
        return toNameList(port -> port.inputName);
    }

    public static List<String> inputNamesTotal() {
        return toNameList(port -> port.inputNameTotal);
    }

    private static List<String> toNameList(Function<DomainPort, String> toNameFunction) {
        return asList(values()).stream()
                .map(toNameFunction)
                .collect(toList());
    }
}