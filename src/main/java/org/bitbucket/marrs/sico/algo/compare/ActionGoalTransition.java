package org.bitbucket.marrs.sico.algo.compare;

import static java.lang.Math.max;

import java.util.ArrayList;
import java.util.Map;
import java.util.stream.DoubleStream;

import org.bitbucket.marrs.sico.data.Edge;
import org.bitbucket.marrs.sico.data.FuzzyWeight;

public final class ActionGoalTransition {

    private final String action;
    private final String goal;
    private final double truthValue;
    private final String actionType;
    private final double transitionWeight;
    private final double goalAmbition;

    public ActionGoalTransition(Edge actionGoalAssociation,
            Map<String, String> actionTypes,
            Map<String, Double> goalAmbitions,
            double featureActionTruthValue) {
//      TODO  this.action = actionGoalAssociation.getNode(actionTypes.keySet());
        this.action = actionGoalAssociation.getSource();
        this.goal = actionGoalAssociation.getOtherNode(action);
        this.truthValue = aggregateTruthValue(actionGoalAssociation.getTruthValue(), featureActionTruthValue);
        this.actionType = actionTypes.get(action);
        this.transitionWeight = actionGoalAssociation.getStrength();
        this.goalAmbition = goalAmbitions.get(goal);
    }

    private double aggregateTruthValue(double... truthValues) {
        double totalUntruth = DoubleStream.of(truthValues).map(val -> 1 - val).sum();

        return max(0, 1 - totalUntruth);
    }

    public String getActionName() {
        return action;
    }

    public String getGoalName() {
        return goal;
    }

    public double getTruthValue() {
        return truthValue;
    }

    public ActionType getActionType() {
        return new ActionType(action, actionType);
    }

    public FuzzyWeight getTransitionWeight() {
        return transitionWeight > 0 ?
                new FuzzyWeight(transitionWeight, 0.0) :
                new FuzzyWeight(0.0, -transitionWeight);
    }

    public double getGoalAmbition() {
        return goalAmbition;
    }

    public static final class ActionType extends ArrayList<String> {
        private static final long serialVersionUID = 1L;

        public ActionType(String name, String type) {
            super(2);
            add(name);
            add(type);
        }

        public String getName() {
            return get(0);
        }

        public String getType() {
            return get(1);
        }
    }

    @Override
    public String toString() {
        return "ActionGoalTransition [action=" + action + ", goal=" + goal + ", truthValue=" + truthValue
                + ", actionType=" + actionType + ", transitionWeight=" + transitionWeight + ", goalAmbition="
                + goalAmbition + "]";
    }
}
