package org.bitbucket.marrs.sico.selemca.entity;

import java.util.Set;

public final class AssociationData {

    private Association association;
    private AssociationMeta metadata;

    public AssociationData() {
        // Support instantiation by reflection
    }

    public AssociationData(Association association, AssociationMeta metadata) {
        this.association = association;
        this.metadata = metadata;
    }

    public Association getAssociation() {
        return association;
    }

    public AssociationMeta getMetadata() {
        return metadata;
    }

    public String getAction(Set<String> actionNames) {
        if (actionNames.contains(association.getConcept1())) {
            return association.getConcept1();
        } else if (actionNames.contains(association.getConcept2())) {
            return association.getConcept2();
        }

        return null;
    }

    @Override
    public String toString() {
        return "AssociationData [association=" + association + ", metadata=" + metadata + "]";
    }
}
