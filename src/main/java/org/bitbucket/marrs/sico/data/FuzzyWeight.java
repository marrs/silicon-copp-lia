package org.bitbucket.marrs.sico.data;

public class FuzzyWeight {
    public static final FuzzyWeight ZERO = new FuzzyWeight(0.0, 0.0);

    private final double positive;
    private final double negative;

    public FuzzyWeight(double positive, double negative) {
        this.positive = checkRange(positive, "positive");
        this.negative = checkRange(negative, "negative");
    }

    public double getPositiveWeight() {
        return positive;
    }

    public double getNegativeWeight() {
        return negative;
    }

    private static double checkRange(double value, String name) {
        if (value < 0.0 || 1.0 < value) {
            throw new IllegalArgumentException(name + " weight out of range: " + value);
        }

        return value;
    }

    @Override
    public String toString() {
        return "FuzzyWeight [positive=" + positive + ", negative=" + negative + "]";
    }
}
