package org.bitbucket.marrs.sico.data.token;

import org.bitbucket.marrs.sico.data.Edge;
import org.bitbucket.marrs.sico.data.type.EdgeType;

import ptolemy.data.ObjectToken;
import ptolemy.data.type.Type;
import ptolemy.kernel.util.IllegalActionException;

public class EdgeToken extends ObjectToken {
    public static final EdgeToken NIL;
    static {
        try {
            NIL = new EdgeToken(null) {
                @Override
                public boolean isNil() {
                    return true;
                }
            };
        } catch (Exception e) {
            // This should never happen.
            throw new RuntimeException(e);
        }
    }

    public EdgeToken(Edge edge) throws IllegalActionException {
        super(edge, Edge.class);
    }

    public static EdgeToken fromEdge(Edge edge) {
        try {
            return new EdgeToken(edge);
        } catch (IllegalActionException e) {
            throw new RuntimeException("edge is always of type Edge.class");
        }
    }

    @Override
    public Type getType() {
        return EdgeType.EDGE;
    }

    public Edge toEdge() {
        return (Edge) getValue();
    }
}
