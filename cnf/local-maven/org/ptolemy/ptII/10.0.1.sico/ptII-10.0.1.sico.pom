<?xml version="1.0" encoding="UTF-8"?>
<project
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"
    xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <modelVersion>4.0.0</modelVersion>
    <groupId>org.ptolemy</groupId>
    <artifactId>ptII</artifactId>
    <version>10.0.1.sico</version>
    <description>POM was created from install:install-file</description>

    <licenses>
        <license>
            <name>BSD License</name>
            <url>http://ptolemy.eecs.berkeley.edu/ptolemyII/ptIIlatest/ptII/copyright.txt</url>
            <distribution>manual</distribution>
            <comments>Ptolemy II is licensed under the BSD License</comments>
        </license>
        <license>
            <name>Aelfred License</name>
            <url>http://ptolemy.eecs.berkeley.edu/ptolemyII/ptIIlatest/ptII/com/microstar/xml/README.txt</url>
            <distribution>manual</distribution>
            <comments>Ptolemy II uses Aelfred to parse xml files.</comments>
        </license>
    </licenses>

    <dependencies>
        <dependency>
            <groupId>antlr</groupId>
            <artifactId>antlr</artifactId>
            <version>[1.9.9,2.7.7]</version>
        </dependency>
        <dependency>
            <groupId>colt</groupId>
            <artifactId>colt</artifactId>
            <version>1.0.3</version>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>10.0.1</version>
        </dependency>
        <dependency>
            <groupId>com.itextpdf</groupId>
            <artifactId>itext-xtra</artifactId>
            <version>5.1.2</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>3.0.1</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>jdom</groupId>
            <artifactId>jdom</artifactId>
            <version>1.0</version>
        </dependency>
        <dependency> <!-- $PTII/org/ptango/lib -->
            <groupId>org.eclipse.jetty</groupId>
            <artifactId>jetty-all</artifactId>
            <version>8.1.5-v20120716</version>
        </dependency>
        <dependency>
            <groupId>org.python</groupId>
            <artifactId>jython-standalone</artifactId>
            <version>2.5.2</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.8.2</version>
        </dependency>
        <dependency>
            <groupId>pl.pragmatists</groupId>
            <artifactId>JUnitParams</artifactId>
            <version>0.3.5</version>
        </dependency>

        <dependency>
            <groupId>net.jini</groupId>
            <artifactId>jini-core</artifactId>
            <version>2.1</version>
        </dependency>
        <dependency>
            <groupId>net.jini</groupId>
            <artifactId>jini-ext</artifactId>
            <version>2.1</version>
        </dependency>
        <dependency>
            <groupId>org.beanshell</groupId>
            <artifactId>bsh</artifactId>
            <version>2.0b4</version>
        </dependency>

        <dependency>
            <groupId>org.mozilla</groupId>
            <artifactId>rhino</artifactId>
            <version>1.7R4</version>
        </dependency>

        <dependency>
            <groupId>org.apache.oltu.oauth2</groupId>
            <artifactId>org.apache.oltu.oauth2.client</artifactId>
            <version>1.0.0</version>
        </dependency>

        <dependency>
            <groupId>org.apache.oltu.oauth2</groupId>
            <artifactId>org.apache.oltu.oauth2.common</artifactId>
            <version>1.0.0</version>
        </dependency>



        <!-- Dependencies that need to be added manually -->
        <!-- See ptII/doc/coding/maven.htm for the commands to run to install these 
            dependencies -->

        <dependency>  <!-- $PTII/lib/jna.jar -->
            <groupId>jna</groupId>
            <artifactId>jna</artifactId>
            <version>4.0.0-variadic</version>
        </dependency>


        <!-- jars have to be taken from ptII/lib -->
        <dependency> <!-- $PTII/lib/sootclasses.jar -->
            <groupId>soot</groupId>
            <artifactId>sootclasses</artifactId>
            <version>ptII</version>
        </dependency>
        <dependency> <!-- $PTII/lib/mapss.jar -->
            <groupId>mapss</groupId>
            <artifactId>mapss</artifactId>
            <version>ptII</version>
        </dependency>
        <dependency> <!-- $PTII/lib/ptCal.jar -->
            <groupId>caltrop</groupId>
            <artifactId>ptCal</artifactId>
            <version>ptII</version>
        </dependency>

        <dependency> <!-- $PTII/lib/chic.jar -->
            <groupId>chic</groupId>
            <artifactId>chic</artifactId>
            <version>ptII</version>
        </dependency>

        <dependency> <!-- $PTII/ptdb/lib/dbxml.jar -->
            <groupId>dbxml</groupId>
            <artifactId>dbxml</artifactId>
            <version>ptII</version>
        </dependency>
        <dependency> <!-- $PTII/ptdb/lib/db.jar -->
            <groupId>dbxml</groupId>
            <artifactId>db</artifactId>
            <version>ptII</version>
        </dependency>

        <dependency> <!-- $PTII/lib/g4ltl.jar -->
            <groupId>g4ltl</groupId>
            <artifactId>g4ltl</artifactId>
            <version>ptII</version>
        </dependency>

        <dependency> <!-- $PTII/lib/kieler.jar -->
            <groupId>kieler</groupId>
            <artifactId>kieler</artifactId>
            <version>ptII</version>
        </dependency>

        <dependency> <!-- $PTII/lib/jsoup-1.7.3.jar -->
            <!-- jsoup HTML parser library @ http://jsoup.org/ -->
            <groupId>org.jsoup</groupId>
            <artifactId>jsoup</artifactId>
            <version>1.7.3</version>
        </dependency>

        <dependency>
            <groupId>oscP5</groupId>
            <artifactId>oscP5</artifactId>
            <version>ptII</version>
        </dependency>

        <dependency> <!-- $PTII/lib/protobuf.jar -->
            <groupId>protobuf</groupId>
            <artifactId>protobuf</artifactId>
            <version>ptII</version>
        </dependency>

        <dependency> <!-- $PTII/lib/ptjacl.jar -->
            <groupId>jacl</groupId>
            <artifactId>jacl</artifactId>
            <version>ptII</version>
        </dependency>

        <dependency>
            <groupId>PDFRenderer</groupId>
            <artifactId>PDFRenderer</artifactId>
            <version>ptII</version>
        </dependency>

        <dependency> <!-- $PTII/lib/smackx.jar -->
            <groupId>jivesoftware</groupId>
            <artifactId>smack</artifactId>
            <version>3.0.4</version>
        </dependency>

        <dependency> <!-- $PTII/lib/smack.jar -->
            <groupId>org.mobicents.slee.resource.smack</groupId>
            <artifactId>smackx</artifactId>
            <version>3.0.4</version>
        </dependency>

        <dependency>
            <groupId>socketio</groupId>
            <artifactId>socketio</artifactId>
            <version>ptII</version>
        </dependency>

        <dependency>
            <groupId>com.itextpdf</groupId>
            <artifactId>itextpdf</artifactId>
            <version>5.5.6</version>
        </dependency>

    </dependencies>
</project>
