Silicon Coppélia
================

Silicon Coppélia is a software for modeling affective decision making.

## Model

The Silicon Coppélia software implements the model described in
[Silicon Coppelia and the Formalization of the Affective Process](https://www.computer.org/csdl/journal/ta/5555/01/09318470/1qdSJmC050c).

Silicon Coppelia software works from the features of the observed other, which need
to be appraised in various domains (e.g., ethics and affordances) before input.
It compares them to provided goals and concerns of the agent, to finally reach a
response that includes intentions to work with the user as well as a level of being
engaged with the other. This ultimately results into a choice among available actions.

## Usage

### Input

Silicon Coppélia expects a single JSON object as input that contains all
parameters needed to execute the model. An example of the
expected JSON input can be found in `data/input.json`.

### REST service

Silicon Coppélia can be started as a REST service:

`./gradlew run`

The service is by default available on `localhost:8080/api/appraise/context` and
expects a POST request with media type `application/json`. An example of the
expected JSON input can be found in `data/input.json`.

On a local machine the REST request can be sent for example using curl:

`curl -H "Content-Type: application/json" --request POST -d "@/absolute/path/to/data/input.json" -sS - "http://localhost:8080/api/appraise/context"`

To test the REST service run the `./scripts/test-sico.sh` script.

### Ptolemy

The software is implemented as a model in the
[Ptolemy](http://ptolemy.eecs.berkeley.edu/) framework.

From the command line the embedded Ptolemy graphical user interface can be
started with the command

`./gradlew runPtolemy`

from the repository root. The model is contained in the `models/` folder and
can be loaded via the _File_ menu in the user interface.

Ptolemy provides further possibilities to execute the model, please refer to the
[Ptolemy documentation](http://ptolemy.eecs.berkeley.edu/ptolemyII/ptIIfaq.htm#invoking%20Ptolemy%20II?)
for further details. Note that in any case Java actors need to be provided to
the Ptolemy classpath (see also the Building section).

In addition to the complete model the software provides actors that implement the
different components of the model. These can be found in the _Silicon Coppélia_
menu point in the graph editor and can be used as building blocks for new
(graphical) models in Ptolemy.

The provided model can be edited in the graphical user interface of Ptolemy.
Some parts of the model that are defined in Java code are not directly editable.
To implement changes in these parts, they need to be copied first. This can be
accomplished by adding a new _Composite actor_ (from the Utilities menu) and
copying the content that needs to be edited into this new actor. Alternatively,
the actor to be edited can be exported as a design pattern into a new xml file.


## Documentation

Documentation of the Silicon Coppélia model can be found in the `doc/` folder.

The software provides Javadoc that is also available in the graphical editor
via the Ptolemy documentation system.


## Building

The software can be built from the command line using Gradle.

### Stand-alone version

A self contained version can be built using the

`./gradlew distZip`

command. This provides a zip archive including an embedded version of Ptolemy
and the Silicon Coppélia model. The archive can be extracted in any folder and
can be run with the included startup scripts by specifying the custom
`configs/configuration.xml` file in the `-configuration` command line parameter,
e.g.

`./bin/sico -configuration configs/configuration.xml`

### Jar archive

In addition, a jar archive can be built that can be used as extension to an
existing Ptolemy installation. The jar archive can be created with the command

`./gradlew ptDoc jar`

To use the actors provided in the created jar archive it needs to be added to
the Ptolemy class path and the actors must be provided to the Ptolemy
configuration, either via a command line argument, as described above, or by
editing the Ptolemy configuration files.

The build also generates Ptolemy specific documentation files. This is done
by the gradle task _ptDoc_, which can be ommited if documentation files are not
needed.

### Docker image

As a further option a Docker image providing the Silicon Coppélia REST API can
be built with the

`./gradlew docker`

command. The REST API is exposed on port 8080 of the Docker container. For
further information see the [plugin](https://github.com/palantir/gradle-docker)
homepage.

To test the Docker setup run

1. `./scripts/docker-run-sico.sh`
2. `./scripts/test-sico.sh`
2. `docker stop sico`


## Development

To support the Ptolemy documentation and export system a couple of conventions
need to be followed when extending or modifying the source code:

- Entities of actors, as e.g. ports or other actors that are part of a composite
actor, must be stored in public non-final fields.

- The name of these entities must match the field name.

- Actors must provide a single argument constructor taking a Workspace as
argument and a two argument constructor taking a CompositeEntity and a String
as name argument.

- To support cloning of actors, which is necessary for exporting, the
clone(Workspace) method must be overridden to copy private fields of an actor
when needed.


## Testing

Test for the software can be executed using Gradle by executing the command

`./gradlew test`

Besides unit test, this also includes integration tests that test the execution
of the implemented actors in Ptolemy and if the provided models executes
without exception. The integration tests depend on example data included in the
`data/` and `models/` directory.


## Known issues

- When copying or exporting parts of the model connections are not drawn. They
must be added manually again.


## Links

* [Source Code](https://bitbucket.org/robopop/silicon-coppelia)
* [Ptolemy](http://ptolemy.eecs.berkeley.edu/)
* [Gradle](https://gradle.org/)


## License

See [LICENSE](https://bitbucket.org/robopop/silicon-coppelia/src/master/LICENSE).


## Authors

* [Johan Hoorn](https://research.vu.nl/en/persons/johan-hoorn)
* [Thomas Baier](https://research.vu.nl/en/persons/thomas-baier)
* [Jeroen van Maanen](https://github.com/jeroenvanmaanen)
* [Jeroen Weseter](https://github.com/GregoryPorter)
* [Marcel Offermans](https://www.luminis.eu/expert/marcel-offermans/)


## References

Johan Hoorn, Thomas Baier, Jeroen van Maanen, Jeroen Wester. (2021). *Silicon Coppelia and the Formalization of the Affective Process.* IEEE Transactions on Affective Computing. PP. 1-1. 10.1109/TAFFC.2020.3048587.
