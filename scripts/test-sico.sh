#!/usr/bin/env bash

host="${1:-localhost:8080}"

BIN="$(cd "$(dirname "$0")" ; pwd)"
PROJECT="$(dirname "${BIN}")"

curl -sS -D - "http://$host/actuator/health"
echo

time curl -H "Content-Type: application/json" --request POST -d "@${PROJECT}/data/input.json" -sS -D - "http://$host/api/appraise/test"
echo
