#!/usr/bin/env bash

port="${1:-8080}"
version="${2:-0.0.1-SNAPSHOT}"

docker run --name sico -p $port:8080 --rm -d "robopop/silicon-coppelia:$version"
